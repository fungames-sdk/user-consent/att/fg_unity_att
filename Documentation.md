# Unity ATT

## Integration Steps

1) Import the **iOS 14 Advertising Support** package from the Package Manager in Unity.

2) **"Install"** or **"Upload"** FG UnityATT plugin from the FunGames Integration Manager in Unity, or download it from here.

3) Click on the **"Prefabs and Settings"** button from FunGames Integration window to fill up your scene with required components and create the Settings asset.